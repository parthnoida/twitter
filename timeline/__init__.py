from flask import Blueprint

timeline = Blueprint('timeline', __name__, url_prefix='/timeline', template_folder="templates", static_folder="static")

from timeline import views
