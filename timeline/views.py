from flask import render_template, current_app, g, request
from flask_login import login_required, current_user
from . import timeline
from models import TimelineRedisOps, TimelineDBOps
from profile.models import ProfileDBOps, ProfileRedisOps
from login.models import LoginDBOps


@timeline.route('/')
@login_required
def index():
    redis_ops = TimelineRedisOps()
    # Getting Tweets from user queue
    timeline_tweets = redis_ops.get_tweets_cache(user_id=current_user.id)
    available_users = LoginDBOps().get_user_list()

    template_values = {
        'timeline_tweets': timeline_tweets,
        'email': current_user.id,
        'available_users': available_users
    }

    return render_template('timeline.html', template_values=template_values)


@timeline.route('/add_tweet/', methods=["POST"])
@login_required
def add_tweet():
    url_data = request.get_json(force=True)
    tweet_text = url_data['tweet_text']

    # add tweet to users db + increment cached counter value
    timeline_db_ops = TimelineDBOps()
    tweet = timeline_db_ops.addTweet(current_user.id, tweet_text)
    ProfileRedisOps().incr_n_tweets(current_user.id)

    if tweet:
        # fetch a list of followers of the user
        follower_ids = ProfileDBOps().get_followers(current_user.id)

        # distribute the tweets to all followers - using async tasks - NEXT ITERATION
        # distribute tweet to all followers - Blocking

        TimelineRedisOps().distribute_tweet_bulk(tweet, follower_ids, current_user)

        return current_app.make_response('success')
    else:
        # send an error message to frontend AJAX - TO IMPLEMENT
        return current_app.make_response('fail')
