import datetime
import pickle

from application import db
from application import redis, r_tweet_pool
from profile.models import ProfileDBOps


class Tweets(db.Document):
    #id = db.ObjectIdField(db_field='_id')
    email = db.StringField(db_field='e')
    post = db.StringField(db_field='p')
    date_created = db.DateTimeField(db_field='dc')

    def __init__(self, email=None, post=None, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        self.email = email
        self.post = post

    def add_date(self):
        self.date_created = datetime.datetime.utcnow()


class TimelineDBOps:

    def __init__(self):
        pass

    def addTweet(self, user_id, post):
        tweet = Tweets(email=user_id, post=post)
        tweet.add_date()
        tweet.save()
        return tweet

    def remove_tweet(self, tweet_id):
        tweet = Tweets.objects(id=tweet_id)
        tweet.delete()

    def get_tweet(self, tweet_id):
        return Tweets.objects.get(id=tweet_id)

    def get_tweets(self, user_id):
        # return an iterable
        return Tweets.objects(email=user_id)

    def get_tweets_bulk(self, user_id_list):
        # return an iterable, so we convert it into a python list of Tweets Object
        return [i for i in Tweets.objects(email__in=user_id_list)]

    def reconstruct_tweet_list(self, user_id):
        profile_db_ops = ProfileDBOps()
        # fetch followings
        following_list = profile_db_ops.get_following(user_id)
        following_list.append(user_id)
        print following_list

        # fetch tweets for all followings - returns a list of Tweets object
        following_tweet_list = self.get_tweets_bulk(following_list)

        # Return Tweet id list
        tweet_id_list = [tweet.id for tweet in following_tweet_list]
        print tweet_id_list
        return following_tweet_list, tweet_id_list


class TimelineRedisOps:
    def __init__(self):
        pass

    def add_tweet_id_to_user_timeline_bulk(self, user_id, tweet_id_list):
        key = 'timeline' + user_id
        pipe = redis.pipeline()
        for tweet_id in tweet_id_list:
            pipe.lpush(key, tweet_id)
        return pipe.execute()

    def add_tweet_to_tweetpool_bulk(self, tweet_list):
        pipe = r_tweet_pool.pipeline()
        for tweet in tweet_list:
            print tweet.post
            # pickling Tweet since its a complex data object
            # setting with an expiry of around 3.8 hours
            pipe.setex(tweet.id, pickle.dumps(tweet), 14000)
        return pipe.execute()

    def distribute_tweet_bulk(self, tweet, follower_list, active_user):
        pipe = redis.pipeline()
        # Adding tweet to our own timeline
        ##pipe.lpush('timeline' + str(current_user.id), tweet.id)
        follower_list.append(active_user)

        # Adding tweet to followers timeline
        for fr in follower_list:
            key = 'timeline' + fr.id
            pipe.lpush(key, tweet.id)
        return pipe.execute()

    def get_tweets_cache(self, user_id):
        key = 'timeline' + user_id
        cache_l = redis.llen(key)
        if cache_l:
            # If tweets are present in cache, fetch
            cached_tweet_ids = redis.lrange(key, 0, cache_l - 1)

            # Each item in tweet_list is of Tweets type
            tweet_list = [x for x in r_tweet_pool.mget(cached_tweet_ids)]

            # Check if we get all tweets from tweet pool
            # fetch tweet pool miss tweets from disk
            profile_db_ops = ProfileDBOps()
            for ix in range(cache_l):
                if tweet_list[ix] is None:
                    temp_tweet = TimelineDBOps().get_tweet(cached_tweet_ids[ix])
                    tweet_list[ix] = temp_tweet
                    # Add tweet to tweet pool
                    r_tweet_pool.setex(temp_tweet.id, pickle.dumps(temp_tweet), 14000)
                else:
                    tweet_list[ix] = pickle.loads(tweet_list[ix])

            return tweet_list
        else:
            # Try to populate tweets from disk
            tweet_list, tweet_id_list = TimelineDBOps().reconstruct_tweet_list(user_id)

            # Add tweets to tweet pool
            self.add_tweet_to_tweetpool_bulk(tweet_list)

            # Add Tweet ids to users timeline queue
            self.add_tweet_id_to_user_timeline_bulk(user_id, tweet_id_list)

            return tweet_list





