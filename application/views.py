from . import main, redis
from flask import current_app, g, redirect, url_for
from flask_login import login_required, current_user


@main.route('/')  # user timeline
@login_required
def index():
    if current_user.is_authenticated:
       return redirect('/timeline/')
    else:
        return redirect('/login/')
        # return current_app.make_response(s)
        #return redirect(url_for('timeline.index', _external=True))


@main.route('/test/')
def test():
    r = redis
    if r:
        cnt = r.incr('main_site_counter')
    else:
        cnt = -1
    return current_app.make_response("Welcome to Twitter MVP, You are visitor no %s :)" % (str(cnt)))
