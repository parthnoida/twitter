import os
from flask import Flask, Blueprint
from config import Config
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt
import redis

app = Flask(__name__, static_folder=os.path.join(os.path.dirname(__file__), "static"))
app.config.from_object(Config)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login.index'

db = MongoEngine(app)

# For global tweet pool
r_tweet_pool = redis.Redis(host='13.59.121.245', port=6379, db=1)
# for usage specific load caches
r_usage = redis.Redis(host='13.59.121.245', port=6379, db=2)
# for user timeline caches, #tweets, #followers, # followings
redis = redis.Redis(host='13.59.121.245', port=6379, db=0)

# Flask BCrypt will be used to salt the user password
flask_bcrypt = Bcrypt(app)

main = Blueprint('main', __name__)
#from views import *
from application import views
from login import login
from profile import profile
from timeline import timeline

app.register_blueprint(main)
app.register_blueprint(login)
app.register_blueprint(profile)
app.register_blueprint(timeline)


