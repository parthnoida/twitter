from application import db
from application import redis


class Following(db.Document):
    email = db.StringField(primary_key=True)
    people = db.ListField(db.StringField(), default=[], db_field='p')


class Followers(db.Document):
    email = db.StringField(primary_key=True)
    people = db.ListField(db.StringField(), default=[], db_field='p')


class ProfileDBOps:

    def __init__(self):
        pass

    def add_follower(self, user_id, follower_id):
        # upsert = add if document does not exists - return updated or added document
        # we are not calling 'update' method on class object since it requires the document to exist first
        # and is not atomic
        return Followers.objects(id=user_id).update_one(add_to_set__people=follower_id, upsert=True)

    def add_following(self, user_id, following_id):
        try:
            return Following.objects(id=user_id).update_one(people=following_id, upsert=True)
        except:
            # if following exists
            following = Following()
            following.email = user_id
            following.people = [following_id]
            following.save()
            return following


    def remove_follower(self, user_id, follower_id):
        return Followers.objects(id=user_id).update(pull__people=follower_id)

    def remove_following(self, user_id, following_id):
        return Following.objects(id=user_id).update(pull__people=following_id)

    def get_followers(self, user_id):
        followers = Followers.objects(email=user_id)
        if followers:
            # returns a list
            return followers[0].people
        return []

    def get_following(self, user_id):
        following = Following.objects(email=user_id)
        if following:
            # returns a list
            return following[0].people
        return []


class ProfileRedisOps:
    def __init__(self):
        pass

    def get_n_tweets(self, email):
        key = 'n_tweet' + email
        n_tweets = redis.get(key)
        if n_tweets:
            return n_tweets
        return 0

    def get_n_followers(self, email):
        key = 'n_followers' + email
        n_followers = redis.get(key)
        if n_followers:
            return n_followers
        return 0

    def get_n_following(self, email):
        key = 'n_following' + email
        n_following = redis.get(key)
        if n_following:
            return n_following
        return 0

    def incr_n_tweets(self, email):
        key = 'n_tweet' + email
        print key
        try:
            n_tweets = redis.incr(key)
            if n_tweets:
                return n_tweets
        except:
            print 'exception in incrementing add tweet count'
        return 0

    def incr_n_followers(self, email):
        key = 'n_followers' + email
        n_followers = redis.incr(key)
        if n_followers:
            return n_followers
        return 0

    def incr_n_following(self, email):
        key = 'n_following' + email
        n_following = redis.incr(key)
        if n_following:
            return n_following
        return 0





