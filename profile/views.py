from flask import render_template, current_app, request
from flask_login import login_required, current_user
from . import profile
from models import ProfileDBOps, ProfileRedisOps
from timeline.models import TimelineDBOps


@profile.route('/<email>')
@login_required
def index(email):
    db_ops = ProfileDBOps()
    # Getting Tweets
    tweets = TimelineDBOps().get_tweets(email)
    tweet_list = []
    for tweet in tweets:
        intr = {}
        intr['post'] = tweet.post
        intr['dt'] = tweet.date_created
        tweet_list.append(intr)

    # Getting Followers, Following
    followers = db_ops.get_followers(email)
    following = db_ops.get_following(email)

    # Getting #followers, #tweets, following from Redis
    redis_ops = ProfileRedisOps()
    n_tweets = redis_ops.get_n_tweets(email)
    n_followers = redis_ops.get_n_followers(email)
    n_following = redis_ops.get_n_following(email)

    # getting follow/following status
    if (email == current_user.id) or (email in db_ops.get_following(current_user.id)):
        follow = 'following'
    else:
        follow = 'follow'

    template_values = {
        'tweet_list': tweet_list,
        'followers': followers,
        'following': following,
        'email': email,
        'n_tweets': n_tweets,
        'n_followers': n_followers,
        'n_following': n_following,
        'follow': follow
    }

    return render_template('profile.html', template_values=template_values)


@profile.route('/toggle_follow/', methods=["POST"])
@login_required
def toggle_follow():
    print current_user.id
    print current_user.email

    url_data = request.get_json(force=True)
    follow_action = url_data['follow']  # can be 'follow' or 'following'
    follow_email = url_data['email']  # email of the person to be followed/unfollowed
    profile_db_ops = ProfileDBOps()
    profile_redis_ops = ProfileRedisOps()

    # Toggling following
    followings = profile_db_ops.get_following(current_user.id)
    if follow_email in followings:
        profile_db_ops.remove_following(current_user.id, following_id=follow_email)
    else:
        profile_db_ops.add_following(current_user.id, following_id=follow_email)

    # toggling Followers
    followers = profile_db_ops.get_followers(follow_email)
    if current_user.id in followers:
        profile_db_ops.remove_follower(follow_email, follower_id=current_user.id)
    else:
        profile_db_ops.add_follower(follow_email, follower_id=current_user.id)

    # Adding Cache Counters
    profile_redis_ops.incr_n_following(current_user.id)
    profile_redis_ops.incr_n_followers(follow_email)

    if follow_action == 'follow':
        return 'following'
    return 'follow'



