from . import login
from flask import render_template, request, redirect, url_for, flash, g
from flask_login import current_user, login_user, logout_user, login_required
from application import lm, flask_bcrypt
from .models import Users

#
# @login.before_request
# def before_request():
#     g.user = current_user


@lm.user_loader
def load_user(email):
    users = Users.objects(__raw__={'_id': email})
    if users:
        return Users.objects(__raw__={'_id': email})[0]
    else:
        return None


@login.route('/')
def index():
    if current_user is not None and current_user.is_authenticated:
        return redirect('/')
    return render_template('login.html')


@login.route('/go/', methods=['POST'])
def signin():
    email = request.form.get('email', None)
    password = request.form.get('password', None)
    valid_users = Users.objects(__raw__={'_id': email})
    for valid_user in valid_users:
        if valid_user.password == password:
            login_user(valid_user)
            flash('Logged in successfully')
            return redirect(url_for('main.index'))
    return redirect(url_for('login.index'))


@login.route('/signup/', methods=['POST'])
def signup():
    name = request.form.get('name')
    email = request.form.get('email')
    password = request.form.get('password')
    user = Users(email, password, name)
    user.save()
    return redirect(url_for('login.index'))


@login.route('/bye/', methods=['GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login.index'))



