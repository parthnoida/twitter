from application import db
import datetime


class Users(db.Document):
    name = db.StringField(db_field='n')
    email = db.StringField(db_field='_id', primary_key=True)
    password = db.StringField(db_field='p')
    date_modified = db.DateTimeField(default=datetime.datetime.now, db_field='dm')

    def __init__(self, email=None, password=None, name=None, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        self.email = email
        self.password = password
        self.name = name

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.email)

    def __repr__(self):
        return '<User %r>' % self.name


class LoginDBOps:
    def __init__(self):
        pass

    def get_user_list(self):
        return Users.objects()


